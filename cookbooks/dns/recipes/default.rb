# -------------------------------------------------------------------------- #
# Copyright 2014-2015, University of Chicago                                 #
#                                                                            #
# Licensed under the Apache License, Version 2.0 (the "License"); you may    #
# not use this file except in compliance with the License. You may obtain    #
# a copy of the License at                                                   #
#                                                                            #
# http://www.apache.org/licenses/LICENSE-2.0                                 #
#                                                                            #
# Unless required by applicable law or agreed to in writing, software        #
# distributed under the License is distributed on an "AS IS" BASIS,          #
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   #
# See the License for the specific language governing permissions and        #
# limitations under the License.                                             #
# -------------------------------------------------------------------------- #

include_recipe "route53"

aws_main = Chef::EncryptedDataBagItem.load("aws", "main")
access_key = aws_main['aws_access_key_id']
secret_key = aws_main['aws_secret_access_key']
aws_route53 = Chef::EncryptedDataBagItem.load("aws","route53")
zoneid = aws_route53['zoneid']
hostname = "#{node['system']['short_hostname']}.#{node['system']['domain_name']}"
public_ipv4 = node[:cloud][:public_ipv4]

route53_record "Create A record" do
  name                  hostname
  value                 public_ipv4
  type                  "A"
  zone_id               zoneid
  aws_access_key_id     access_key
  aws_secret_access_key secret_key
  overwrite             true
  action                :create
end

# -------------------------------------------------------------------------- #
# Copyright 2014-2015, University of Chicago                                 #
#                                                                            #
# Licensed under the Apache License, Version 2.0 (the "License"); you may    #
# not use this file except in compliance with the License. You may obtain    #
# a copy of the License at                                                   #
#                                                                            #
# http://www.apache.org/licenses/LICENSE-2.0                                 #
#                                                                            #
# Unless required by applicable law or agreed to in writing, software        #
# distributed under the License is distributed on an "AS IS" BASIS,          #
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   #
# See the License for the specific language governing permissions and        #
# limitations under the License.                                             #
# -------------------------------------------------------------------------- #


directory "/etc/grid-security" do
  owner "root"
  group "root"
  mode "0755"
  action :create
end

directory "/etc/grid-security/certificates" do
  owner "root"
  group "root"
  mode "0755"
  action :create
end

cookbook_file "/etc/grid-security/certificates/7a42187f.0" do
  source "7a42187f.0"
  mode 0644
  owner "root"
  group "root"
end

cookbook_file "/etc/grid-security/certificates/7a42187f.signing_policy" do
  source "7a42187f.signing_policy"
  mode 0644
  owner "root"
  group "root"
end

cookbook_file "/etc/grid-security/certificates/AddTrustExternalCARoot.pem" do
  source "AddTrustExternalCARoot.pem"
  mode 0644
  owner "root"
  group "root"
end

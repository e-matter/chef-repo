# -------------------------------------------------------------------------- #
# Copyright 2014-2015, University of Chicago                                 #
#                                                                            #
# Licensed under the Apache License, Version 2.0 (the "License"); you may    #
# not use this file except in compliance with the License. You may obtain    #
# a copy of the License at                                                   #
#                                                                            #
# http://www.apache.org/licenses/LICENSE-2.0                                 #
#                                                                            #
# Unless required by applicable law or agreed to in writing, software        #
# distributed under the License is distributed on an "AS IS" BASIS,          #
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   #
# See the License for the specific language governing permissions and        #
# limitations under the License.                                             #
# -------------------------------------------------------------------------- #


require "openssl"

include_recipe "globus::go-cert"

package "gsi-openssh-clients"

ep_name = "#{node[:system][:short_hostname]}.#{node[:system][:domain_name]}"
server = "#{node[:system][:short_hostname]}.#{node[:system][:domain_name]}"

globus_cli_cred = Chef::EncryptedDataBagItem.load("globus", "cli_cred")
username = globus_cli_cred['username']
ssh_key = globus_cli_cred['ssh_key']

template "/root/.ssh/id_rsa_go" do
  source "id_rsa_go.erb"
  mode 0400
  owner "root"
  group "root"
  variables(
    :ssh_key => ssh_key
  )
end

cookbook_file "/etc/grid-security/anon.cert" do
  source "anon.cert"
  mode 0644
  owner "root"
  group "root"
end

cookbook_file "/etc/grid-security/anon.key" do
  source "anon.key"
  mode 0400
  owner "root"
  group "root"
end

ruby_block "set_go_endpoint" do
  block do
    begin
      uuid_re = /[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}/
      out = `ssh -i /root/.ssh/id_rsa_go -l #{username} -o "StrictHostKeyChecking no" cli.globusonline.org endpoint-remove #{ep_name} 2>/dev/null`
      setup_key_blob = `ssh -i /root/.ssh/id_rsa_go -l #{username} -o "StrictHostKeyChecking no" cli.globusonline.org endpoint-add --gc #{ep_name}`
      setup_key = uuid_re.match(setup_key_blob)

      cert_file = "/etc/grid-security/gc-cert.pem"
      key_file = "/etc/grid-security/gc-key.pem"
      ENV["X509_USER_CERT"]="/etc/grid-security/anon.cert"
      ENV["X509_USER_KEY"]="/etc/grid-security/anon.key"
      cert_blob = `gsissh -F /dev/null -o "GSSApiTrustDns no" -o "ServerAliveInterval 15" -o "ServerAliveCountMax 8" relay.globusonline.org -p 2223 register #{setup_key}`

      cert = OpenSSL::X509::Certificate.new(cert_blob)
      cert_f = File.new(cert_file, 'w')
      cert_f.write(cert.to_pem)
      cert_f.chmod(0644)

      key = OpenSSL::PKey::RSA.new(cert_blob)
      key_f = File.new(key_file, 'w')
      key_f.write(key.to_pem)
      key_f.chmod(0400)

      out = `ssh -i /root/.ssh/id_rsa_go -l #{username} -o "StrictHostKeyChecking no" cli.globusonline.org endpoint-remove #{ep_name}`
      subject = cert.subject
      out = `ssh -i /root/.ssh/id_rsa_go -l #{username} -o "StrictHostKeyChecking no" cli.globusonline.org "endpoint-add #{ep_name} -p #{server} -s '#{subject}'"`
      out = `ssh -i /root/.ssh/id_rsa_go -l #{username} -o "StrictHostKeyChecking no" cli.globusonline.org "endpoint-modify #{ep_name} --myproxy-server=myproxy.globusonline.org"`
      out = `ssh -i /root/.ssh/id_rsa_go -l #{username} -o "StrictHostKeyChecking no" cli.globusonline.org "endpoint-modify #{ep_name} --public"`
    rescue
      p "Unable to obtain GC certificate"
    end
  end
end

template "/etc/gridftp.conf" do
  source "gridftp.conf.erb"
  mode 0644
  owner "root"
  group "root"
  variables(
      :public_ip => node["cloud"]["public_ipv4"],
  )
  notifies :restart, "service[globus-gridftp-server]"
end

service "globus-gridftp-server"

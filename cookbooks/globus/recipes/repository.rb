# -------------------------------------------------------------------------- #
# Copyright 2014-2015, University of Chicago                                 #
#                                                                            #
# Licensed under the Apache License, Version 2.0 (the "License"); you may    #
# not use this file except in compliance with the License. You may obtain    #
# a copy of the License at                                                   #
#                                                                            #
# http://www.apache.org/licenses/LICENSE-2.0                                 #
#                                                                            #
# Unless required by applicable law or agreed to in writing, software        #
# distributed under the License is distributed on an "AS IS" BASIS,          #
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   #
# See the License for the specific language governing permissions and        #
# limitations under the License.                                             #
# -------------------------------------------------------------------------- #

#Chef::Log.level = :debug

case node.platform
  when "ubuntu"
    if node.platform_version == "12.04"
      distro_id = "precise"
    elsif node.platform_version == "12.10"
      distro_id = "quantal"
    elsif node.platform_version == "13.04"
      distro_id = "raring"
    elsif node.platform_version == "13.10"
      distro_id = "saucy"
    elsif node.platform_version == "14.04"
      distro_id = "trusty"
    elsif node.platform_version == "14.10"
      distro_id = "utopic"
    end
end

remote_file "/tmp/gt5_repository.deb" do
  action :create_if_missing 
  source "http://www.globus.org/ftppub/gt5/5.2/stable/installers/repo/globus-repository-5.2-stable-#{distro_id}_0.0.3-1_all.deb"
  owner "root"
  group "root"
  mode "0644"
end

package "/tmp/gt5_repository" do
  action :install
  source "/tmp/gt5_repository.deb"
  provider Chef::Provider::Package::Dpkg
  notifies :run, "execute[apt-get update]", :immediately
end

execute "apt-get update" do
  user "root"
  group "root"
  command "apt-get update"
  action :nothing
end

name             'globus'
maintainer       'University of Chicago'
maintainer_email 'lukasz@uchicago.edu'
license          'All rights reserved'
description      'Installs/Configures globus'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'

name             'ematter'
maintainer       'University of Chicago'
maintainer_email 'karpeev@uchicago.edu'
license          'All rights reserved'
description      'Installs/Configures ematter'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'

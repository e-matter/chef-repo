# CHANGELOG for ematter

This file is used to list changes made in each version of ematter.

## 0.1.0:

* Initial release of ematter

- - -
Check the [Markdown Syntax Guide](http://daringfireball.net/projects/markdown/syntax) for help with Markdown.

The [Github Flavored Markdown page](http://github.github.com/github-flavored-markdown/) describes the differences between markdown on github and standard markdown.

# -------------------------------------------------------------------------- #
# Copyright 2014-2015, University of Chicago                                 #
#                                                                            #
# Licensed under the Apache License, Version 2.0 (the "License"); you may    #
# not use this file except in compliance with the License. You may obtain    #
# a copy of the License at                                                   #
#                                                                            #
# http://www.apache.org/licenses/LICENSE-2.0                                 #
#                                                                            #
# Unless required by applicable law or agreed to in writing, software        #
# distributed under the License is distributed on an "AS IS" BASIS,          #
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   #
# See the License for the specific language governing permissions and        #
# limitations under the License.                                             #
# -------------------------------------------------------------------------- #

package 'tcl8.5'
package 'libx11-dev'
package 'python-pyside'
package 'python-matplotlib'
package 'python-yaml'
package 'gfortran'
package 'git'
package 'm4'

# TODO: how do we parameterize the package name by the Ubuntu version?
remote_file "/tmp/moose-environment_ubuntu_14.04_1.1-3.x86_64.deb" do
  source "http://www.mooseframework.com/static/media/uploads/files/moose-environment_ubuntu_14.04_1.1-3.x86_64.deb"
  mode 0644
  #checksum "" # PUT THE SHA256 CHECKSUM HERE
end

dpkg_package "moose-environment" do
  source "/tmp/moose-environment_ubuntu_14.04_1.1-3.x86_64.deb"
  action :install
end


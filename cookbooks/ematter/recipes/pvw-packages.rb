# -------------------------------------------------------------------------- #
# Copyright 2014-2015, University of Chicago                                 #
#                                                                            #
# Licensed under the Apache License, Version 2.0 (the "License"); you may    #
# not use this file except in compliance with the License. You may obtain    #
# a copy of the License at                                                   #
#                                                                            #
# http://www.apache.org/licenses/LICENSE-2.0                                 #
#                                                                            #
# Unless required by applicable law or agreed to in writing, software        #
# distributed under the License is distributed on an "AS IS" BASIS,          #
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   #
# See the License for the specific language governing permissions and        #
# limitations under the License.                                             #
# -------------------------------------------------------------------------- #

# Pare down the packages that are not needed once PVW has been built.
package 'git'
package 'libxt6'
package 'libapr1-dev'
package 'mesa-common-dev'
package 'libxt-dev'
package 'python2.7-dev'
package 'libglu1-mesa'
package 'libxrender1'
package 'libfontconfig1'
package 'libxml2'
package 'libxml2-utils'
package 'python-libxml2'
package 'autoconf'
package 'automake'
package 'pkg-config'
package 'libtool'



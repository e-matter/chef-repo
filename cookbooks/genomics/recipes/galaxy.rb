# -------------------------------------------------------------------------- #
# Copyright 2014-2015, University of Chicago                                 #
#                                                                            #
# Licensed under the Apache License, Version 2.0 (the "License"); you may    #
# not use this file except in compliance with the License. You may obtain    #
# a copy of the License at                                                   #
#                                                                            #
# http://www.apache.org/licenses/LICENSE-2.0                                 #
#                                                                            #
# Unless required by applicable law or agreed to in writing, software        #
# distributed under the License is distributed on an "AS IS" BASIS,          #
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   #
# See the License for the specific language governing permissions and        #
# limitations under the License.                                             #
# -------------------------------------------------------------------------- #

galaxy_dir = node['galaxy']['dir']
galaxy_repo = node['galaxy']['repo']
galaxy_tarball = "#{node[:galaxy][:revision]}.tar.bz2"
scratch_dir = "/tmp"
universe = node['galaxy']['universe']


globus_cli_cred = Chef::EncryptedDataBagItem.load("globus", "cli_cred")
globus_username = globus_cli_cred['username']
globus_epname = "#{node[:system][:short_hostname]}.#{node[:system][:domain_name]}"

postgres_name = node[:system][:short_hostname]

package "python-setuptools"

# Install GO Transfer API
execute "install-transfer-api" do
  user "root"
  group "root"
  command "easy_install-2.7 globusonline-transfer-api-client"
  action :run
end

# Install Python Nexus Client
execute "wget http://github.com/globusonline/python-nexus-client/archive/JIRA-GRAPH-1069-SIGNOFF-1.tar.gz" do
  cwd "#{scratch_dir}"
  action :run
end

execute "tar xzf JIRA-GRAPH-1069-SIGNOFF-1.tar.gz" do
  cwd "#{scratch_dir}"
  action :run
end

# setup.py has to be run three times to succeed. It needs to be investigated to fix it.
execute "python setup.py install JIRA-GRAPH-1069-SIGNOFF-1" do
  cwd "#{scratch_dir}/python-nexus-client-JIRA-GRAPH-1069-SIGNOFF-1"
  command "python setup.py install"
  action :run
  returns [0, 1]
end
execute "python setup.py install JIRA-GRAPH-1069-SIGNOFF-1" do
  cwd "#{scratch_dir}/python-nexus-client-JIRA-GRAPH-1069-SIGNOFF-1"
  command "python setup.py install"
  action :run
  returns [0, 1]
end
execute "python setup.py install JIRA-GRAPH-1069-SIGNOFF-1" do
  cwd "#{scratch_dir}/python-nexus-client-JIRA-GRAPH-1069-SIGNOFF-1"
  command "python setup.py install"
  action :run
  returns [0, 1]
end

# Install Galaxy

directory "#{node[:filesystem][:scratch_mount_point]}/galaxy" do
  owner "galaxy"
  group "galaxy"
  mode "0755"
  action :create
end

directory "#{node[:filesystem][:scratch_mount_point]}/go" do
  owner "root"
  group "root"
  mode "1777"
  action :create
end

directory "#{galaxy_dir}" do
  owner "galaxy"
  group "galaxy"
  mode "0755"
  action :create
end

remote_file "#{scratch_dir}/#{galaxy_tarball}" do
  source "#{galaxy_repo}/#{galaxy_tarball}"
  owner "root"
  group "root"
  mode "0644"
end

execute "tar" do
  user "galaxy"
  group "galaxy"
  command "tar xjf #{scratch_dir}/#{galaxy_tarball} --strip-components=1 --directory #{galaxy_dir}"
  action :run
end

cookbook_file "#{galaxy_dir}/galaxy-setup.sh" do
  source "galaxy-setup.sh"
  owner "galaxy"
  group "galaxy"
  mode "0755"
end

port = 8080

template "#{galaxy_dir}/universe_wsgi.ini" do
  source "universe_wsgi.ini.erb"
  owner "galaxy"
  group "galaxy"
  mode "0644"
  variables(
      :port => port,
      :db_connect => "postgres:///galaxy_#{postgres_name}?user=galaxy&password=galaxy&host=/var/run/postgresql",
      :globus_username => globus_username,
      :globus_epname => globus_epname,
  )
end

link "#{galaxy_dir}/tool_conf.xml" do
  to "#{galaxy_dir}/tool_conf_#{universe}.xml"
end

# Setup Galaxy
execute "galaxy-setup.sh" do
  user "galaxy"
  group "galaxy"
  cwd galaxy_dir
  command "./galaxy-setup.sh"
  action :run
end

# Add init script
cookbook_file "/etc/init.d/galaxy" do
  source "galaxy.init"
  owner "root"
  group "root"
  mode "0755"
  notifies :run, "execute[update-rc.d]"
end

execute "update-rc.d" do
  user "root"
  group "root"
  command "update-rc.d galaxy defaults"
  action :nothing
end

execute "galaxy_restart" do
 user "root"
 group "root"
 command "/etc/init.d/galaxy restart"
 action :run
 environment ({'PATH' => "/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"})
end

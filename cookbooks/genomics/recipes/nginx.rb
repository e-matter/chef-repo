# -------------------------------------------------------------------------- #
# Copyright 2014-2015, University of Chicago                                 #
#                                                                            #
# Licensed under the Apache License, Version 2.0 (the "License"); you may    #
# not use this file except in compliance with the License. You may obtain    #
# a copy of the License at                                                   #
#                                                                            #
# http://www.apache.org/licenses/LICENSE-2.0                                 #
#                                                                            #
# Unless required by applicable law or agreed to in writing, software        #
# distributed under the License is distributed on an "AS IS" BASIS,          #
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   #
# See the License for the specific language governing permissions and        #
# limitations under the License.                                             #
# -------------------------------------------------------------------------- #


hostname = node['system']['short_hostname']
domainname = node['system']['domain_name']
server = "#{hostname}.#{domainname}"
execute_dir = node['condor']['execute_dir']

#package "nginx-extras"

directory "/etc/nginx/certs" do
  owner "root"
  group "root"
  mode "0755"
end

link "/etc/nginx/certs/globusgenomics.org.key" do
  to "/etc/ssl/private/ssl-cert-snakeoil.key"
end

link "/etc/nginx/certs/globusgenomics.org.crt" do
  to "/etc/ssl/certs/ssl-cert-snakeoil.pem"
end

port = 8080

template "/etc/nginx/sites-available/galaxy" do
  source "nginx_galaxy.erb"
  owner "root"
  group "root"
  mode "0644"
  variables(
    :port => port,
    :server => hostname,
    :domain => domainname
  )
end

link "/etc/nginx/sites-enabled/galaxy" do
  to "/etc/nginx/sites-available/galaxy"
end

service "nginx" do
    action :restart
end

# For IGV
directory "/usr/share/nginx/wwwTrue" do
  mode "0755"
  owner "root"
  group "root"
  recursive true
  action :create
end

link "/usr/share/nginx/wwwTrue/scratch" do
  owner "root"
  group "root"
  to "/scratch"
  action :create
end

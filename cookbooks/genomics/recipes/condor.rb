# -------------------------------------------------------------------------- #
# Copyright 2014-2015, University of Chicago                                 #
#                                                                            #
# Licensed under the Apache License, Version 2.0 (the "License"); you may    #
# not use this file except in compliance with the License. You may obtain    #
# a copy of the License at                                                   #
#                                                                            #
# http://www.apache.org/licenses/LICENSE-2.0                                 #
#                                                                            #
# Unless required by applicable law or agreed to in writing, software        #
# distributed under the License is distributed on an "AS IS" BASIS,          #
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   #
# See the License for the specific language governing permissions and        #
# limitations under the License.                                             #
# -------------------------------------------------------------------------- #

name = node['name']
hostname = node['system']['short_hostname']
domainname = node['system']['domain_name']
server = "#{hostname}.#{domainname}"
execute_dir = node['condor']['execute_dir']

# We cannot use apt_repository because it adds 'deb-src' that is not provided by the repo.
execute "add Condor repository" do
  user "root"
  group "root"
  command "/bin/echo 'deb [arch=amd64] http://www.cs.wisc.edu/condor/debian/stable/ wheezy contrib' > /etc/apt/sources.list.d/condor.list"
  action :run
end

execute "update Condor repo" do
  user "root"
  group "root"
  command "apt-get update"
  action :run
end

package "condor" do
  options "--force-yes"
  action :install
end

execute "update-rc.d condor defaults" do
  user "root"
  group "root"
end

# Create EXECUTE directory. The directory is used by local STARTD must not
# be shared with other STARTD.

directory execute_dir do
  mode 0755
  owner "condor"
  group "condor"
  recursive true
end

# Create the local configuration file.

template "/etc/condor/condor_config.local" do
  source "condor_config.erb"
  mode 0644
  owner "condor"
  group "condor"
  variables(
    :server => server,
    :domain => domainname,
    :daemons => "COLLECTOR, MASTER, NEGOTIATOR, SCHEDD, STARTD"
  )
  notifies :restart, "service[condor]"
end

service "condor"

# Install scripts to manage condor pool

directory "/opt/scripts" do
  owner "root"
  group "root"
  mode 0755
  action :create
end

template "/opt/scripts/cloudinit.cfg_template" do
  source "cloudinit.cfg_template.erb"
  owner "root"
  group "root"
  mode 0644
  variables(
    :domainname => domainname
  )
end

aws_main = Chef::EncryptedDataBagItem.load("aws", "main")
aws_access_key = aws_main['aws_access_key_id']
aws_secret_key = aws_main['aws_secret_access_key']

template "/opt/scripts/manage_dynamic_pool.sh" do
  source "manage_dynamic_pool.sh.erb"
  owner "root"
  group "root"
  mode 0755
  variables(
    :aws_access_key => aws_access_key,
    :aws_secret_key => aws_secret_key,
  )
  action :create
end

template "/etc/cron.d/manage_dynamic_pool" do
  source "manage_dynamic_pool.erb"
  mode 0644
  owner "root"
  group "root"
  variables(
    :name => hostname,
  )
end

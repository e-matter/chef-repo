# -------------------------------------------------------------------------- #
# Copyright 2014-2015, University of Chicago                                 #
#                                                                            #
# Licensed under the Apache License, Version 2.0 (the "License"); you may    #
# not use this file except in compliance with the License. You may obtain    #
# a copy of the License at                                                   #
#                                                                            #
# http://www.apache.org/licenses/LICENSE-2.0                                 #
#                                                                            #
# Unless required by applicable law or agreed to in writing, software        #
# distributed under the License is distributed on an "AS IS" BASIS,          #
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   #
# See the License for the specific language governing permissions and        #
# limitations under the License.                                             #
# -------------------------------------------------------------------------- #


tools_snapshot = node['filesystem']['tools_snapshot']
tools_mount_point = node['filesystem']['tools_mount_point']
tools_size = node['filesystem']['tools_size']
indices_snapshot = node['filesystem']['indices_snapshot']
indices_mount_point = node['filesystem']['indices_mount_point']
indices_size = node['filesystem']['indices_size']
ematter_snapshot = node['filesystem']['ematter_snapshot']
ematter_mount_point = node['filesystem']['ematter_mount_point']
ematter_size = node['filesystem']['ematter_size']

scratch_size = node['filesystem']['scratch_size']
scratch_type = node['filesystem']['scratch_type']
scratch_mount_point = node['filesystem']['scratch_mount_point']

package "lvm2"
package "ec2-api-tools"

include_recipe "aws"
aws = data_bag_item("aws", "main")


# Unmount /dev/xvdb mounted by default at /mnt

mount "mnt" do
  device "/dev/xvdb"
  action [ :umount, :disable ]
end

# Galaxy Tools

directory tools_mount_point do
  mode 0755
  owner "root"
  group "root"
end

aws_main = Chef::EncryptedDataBagItem.load("aws", "main")
access_key = aws_main['aws_access_key_id']
secret_key = aws_main['aws_secret_access_key']

aws_ebs_volume "tools_volume" do
  aws_access_key access_key
  aws_secret_access_key secret_key
  snapshot_id tools_snapshot
  size tools_size
  device "/dev/sdg1"
  action [ :create, :attach ]
end

mount tools_mount_point do
  device "/dev/xvdg1"
  options "rw noatime"
  action [ :enable, :mount ]
end

# Galaxy Indices

directory indices_mount_point do
  mode 0755
  owner "root"
  group "root"
end

aws_ebs_volume "indices_volume" do
  aws_access_key access_key
  aws_secret_access_key secret_key
  snapshot_id indices_snapshot
  size indices_size
  device "/dev/sdg2"
  action [ :create, :attach ]
end

mount indices_mount_point do
  device "/dev/xvdg2"
  options "rw noatime"
  action [ :enable, :mount ]
end

# ematter executables

directory ematter_mount_point do
  mode 0755
  owner "root"
  group "root"
end

aws_ebs_volume "ematter_volume" do
  aws_access_key access_key
  aws_secret_access_key secret_key
  snapshot_id ematter_snapshot
  size ematter_size
  device "/dev/sdg3"
  action [ :create, :attach ]
end

mount ematter_mount_point do
  device "/dev/xvdg3"
  options "rw noatime"
  action [ :enable, :mount ]
end

# GO and Galaxy scratch

include_recipe "lvm"

directory scratch_mount_point do
  mode 0755
  owner "root"
  group "root"
end

aws_ebs_volume "scratch_volume1" do
  aws_access_key access_key
  aws_secret_access_key secret_key
  size scratch_size/2
  device "/dev/sdf1"
  action [ :create, :attach ]
end

aws_ebs_volume "scratch_volume2" do
  aws_access_key access_key
  aws_secret_access_key secret_key
  size scratch_size/2
  device "/dev/sdf2"
  action [ :create, :attach ]
end

lvm_physical_volume "/dev/xvdf1"
lvm_physical_volume "/dev/xvdf2"

lvm_volume_group "scratchvg" do
  physical_volumes [ "/dev/xvdf1", "/dev/xvdf2" ]
  logical_volume "scratchlv" do
    size "100%VG"
    filesystem "ext4"
    mount_point scratch_mount_point
    stripes 2
    stripe_size 64
  end
end


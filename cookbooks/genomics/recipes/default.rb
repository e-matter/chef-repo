# -------------------------------------------------------------------------- #
# Copyright 2014-2015, University of Chicago                                 #
#                                                                            #
# Licensed under the Apache License, Version 2.0 (the "License"); you may    #
# not use this file except in compliance with the License. You may obtain    #
# a copy of the License at                                                   #
#                                                                            #
# http://www.apache.org/licenses/LICENSE-2.0                                 #
#                                                                            #
# Unless required by applicable law or agreed to in writing, software        #
# distributed under the License is distributed on an "AS IS" BASIS,          #
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   #
# See the License for the specific language governing permissions and        #
# limitations under the License.                                             #
# -------------------------------------------------------------------------- #

include_recipe "users"

users_manage "galaxy" do
end

key_file = "/home/galaxy/.ssh/id_rsa"

execute "ssh-keygen galaxy" do
  not_if do File.exists?(key_file) end
  user "galaxy"
  command "ssh-keygen -N \"\" -f #{key_file}"
  action :run
end

# Optimize kernel for EBS storage
cookbook_file "/etc/sysctl.d/60-ebs.conf" do
  source "60-ebs.conf"
  owner "root"
  group "root"
  mode "0644"
end

execute "sysctl -p /etc/sysctl.d/60-ebs.conf" do
  user "root"
  group "root"
  action :run
end


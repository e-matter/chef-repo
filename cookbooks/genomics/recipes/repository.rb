# -------------------------------------------------------------------------- #
# Copyright 2014-2015, University of Chicago                                 #
#                                                                            #
# Licensed under the Apache License, Version 2.0 (the "License"); you may    #
# not use this file except in compliance with the License. You may obtain    #
# a copy of the License at                                                   #
#                                                                            #
# http://www.apache.org/licenses/LICENSE-2.0                                 #
#                                                                            #
# Unless required by applicable law or agreed to in writing, software        #
# distributed under the License is distributed on an "AS IS" BASIS,          #
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   #
# See the License for the specific language governing permissions and        #
# limitations under the License.                                             #
# -------------------------------------------------------------------------- #


include_recipe "apt"

case node.platform
  when "ubuntu"
    if node.platform_version == "12.04"
      distro_id = "precise"
    elsif node.platform_version == "12.10"
      distro_id = "quantal"
    elsif node.platform_version == "13.04"
      distro_id = "raring"
    elsif node.platform_version == "13.10"
      distro_id = "saucy"
    elsif node.platform_version == "14.04"
      distro_id = "trusty"
    elsif node.platform_version == "14.10"
      distro_id = "utopic"
    end
end

apt_repository "multiverse" do
  uri "http://us-east-1.ec2.archive.ubuntu.com/ubuntu/"
  arch "amd64"
  distribution distro_id
  components [ "multiverse" ]
end

apt_repository "multiverse-updates" do
  uri "http://us-east-1.ec2.archive.ubuntu.com/ubuntu/"
  arch "amd64"
  distribution "#{distro_id}-updates"
  components [ "multiverse" ]
end

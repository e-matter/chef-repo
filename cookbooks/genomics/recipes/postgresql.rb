# -------------------------------------------------------------------------- #
# Copyright 2014-2015, University of Chicago                                 #
#                                                                            #
# Licensed under the Apache License, Version 2.0 (the "License"); you may    #
# not use this file except in compliance with the License. You may obtain    #
# a copy of the License at                                                   #
#                                                                            #
# http://www.apache.org/licenses/LICENSE-2.0                                 #
#                                                                            #
# Unless required by applicable law or agreed to in writing, software        #
# distributed under the License is distributed on an "AS IS" BASIS,          #
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   #
# See the License for the specific language governing permissions and        #
# limitations under the License.                                             #
# -------------------------------------------------------------------------- #


name = node['system']['short_hostname']

package "postgresql-client-common"
package "postgresql"

database_exists = "psql galaxy postgres -c ''"

# Create postgresql user

execute "createuser" do
  user "postgres"
  command "createuser -D -S -R galaxy"
  action :run
  not_if database_exists
end

execute "alter_user" do
  user "postgres"
  command "psql -c \"alter user galaxy with encrypted password 'galaxy';\""
  action :run
  not_if database_exists
end

# Create the galaxy database

execute "createdb_server" do
  user "postgres"
  command "createdb galaxy_#{name}"
  action :run
  not_if "psql galaxy postgres -c ''"
end

execute "grant_all_servers" do
  user "postgres"
  command "psql -c \"grant all privileges on database galaxy_#{name} to galaxy;\""
  action :run
  not_if "psql galaxy postgres -c ''"
end

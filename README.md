Globus eMatter
==================

The documents describes how to launch a Globus eMatter instance using Opscode Chef.

## Prerequisites

To launch Globus eMatter instances, the following elements of the infrastructure have to be set up first:

* On your local machine: install the `knife` command-line tool and the `knife-ec2` plugin.
  * The easiest way to install is [ChefDK](https://downloads.getchef.com/chef-dk/). Run the installer, then
  * `chef gem install knife-ec2`
  * The `knife` command and `knife ec2` subcommands should now be installed.
* A **Chef Server** instance — we currently use Opscode's hosted Chef server at [manage.opscode.com](http://manage.opscode.com). Through the web interface, you can join an organization, and then download the organization’s private key (`<ORGANIZATION>-validator.pem`), and your user key (`<CHEF-USERNAME>.pem`). Both keys have to be saved in the `.chef/` subdirectory and referenced in the `.chef/knife.rb` file.
* An **AWS account**
  * Grab your AWS credentials and set them as environment variables like so: `export AWS_ACCESS_KEY_ID=<...>; export AWS_SECRET_ACCESS_KEY=<...>`. Chef will use these to launch your instance. You will also need to set your credentials by editing the "aws" data bag, see below.
  * Create a **security group** that allows 0.0.0.0/0 to access ports 22, 80, 443, 2811, 50000-51000, and opens all ports between instances launched by the same Amazon user. Its name will be referenced in the instance creation command below.
* A **Globus account** with an access to the CLI configured. An SSH private key used to authenticate to the CLI must be unencrypted. Globus eMatter Galaxy leverages OAuth protocol to verify a user identity against Globus OAuth server. Globus OAuth server accepts redirects only from whitelisted domains. To be able to use Globus eMatter Galaxy, you need to send a request to Globus team to add your domain to the OAuth whitelist.
* Optionally an SMTP Relay Server with SASL authentication.

The AWS, Globus, and SASL credentials must be stored in encrypted data bags.

## Globus eMatter Chef Repo

[This repo](https://github.com/globusonline/genomics-chef-repo) stores the specific cookbooks, data bags, roles, etc for launching a GG instance. Get the latest revision of the repo:

```
$ git clone git@github.com:globusonline/genomics-chef-repo.git
$ cd genomics-chef-repo/
```

Create `.chef/` directory and copy to the directory:

* your private key (`<CHEF-USERNAME>.pem`, downloaded from Chef server web interface)
your organization’s private key (`<ORGANIZATION>-validator.pem`, also downloaded from server),
* knife’s configuration (`knife.rb`) e.g.:

```
current_dir = File.dirname(__FILE__)
log_level                :info
log_location             STDOUT
node_name                "lukasz"
client_key               "#{current_dir}/lukasz.pem"
validation_client_name   "galaxycloud-validator"
validation_key           "#{current_dir}/galaxycloud-validator.pem"
chef_server_url          "https://api.opscode.com/organizations/galaxycloud"
cache_type               'BasicFile'
cache_options( :path => "#{ENV['HOME']}/.chef/checksums" )
cookbook_path            ["#{current_dir}/../cookbooks"]

cookbook_copyright      "University of Chicago"
cookbook_email          "lukasz@uchicago.edu"

knife[:secret_file] = "#{current_dir}/encrypted_data_bag_secret"

# Amazon AWS
knife[:aws_access_key_id] = ENV['AWS_ACCESS_KEY_ID']
knife[:aws_secret_access_key] = ENV['AWS_SECRET_ACCESS_KEY']
knife[:flavor] = "m1.small"
# Ubuntu 14.04 LTS Trusty
knife[:image] = "ami-018c9568"
knife[:aws_ssh_key_id] = "ggtest"
```

* a symmetric key to encrypt/decrypt data bags (`encrypted_data_bag_secret`).

## Launching instances

* Modify `roles/globusgenomics.json` according to a requested scratch filesystem size, hostname, domain name, Globus username, etc. This file also lets you specify a tool snapshot id.
* Upload all cookbooks and data bags to the Chef server:
```
$ knife upload .
```
* Modify "aws" and "globus" data bags to set proper credentials:
```
$ knife data bag edit aws main
$ knife data bag edit globus cli_cred
```
* Launch a new instance:

```
$ knife ec2 server create -i <AWS_private_key_path> -g <AWS_security_group_name> -x ubuntu -r 'role[globusgenomics]' -N <new_node_name>
```

  (`-x` specifies the SSH username Knife will use to bootstrap the node. More options documented [here](https://docs.getchef.com/plugin_knife_ec2.html))

  This command creates the EC2 instance, SSHs into it, and installs and starts Chef client. Chef client connects to the Chef server, downloads cookbooks, and executes recipes in the run list for the "globusgenomics" role.

* Create an A record that maps `node['system']['short_hostname'].node['system']['domain_name']` to a public IP address of the launched instance.
